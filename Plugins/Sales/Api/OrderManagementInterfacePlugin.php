<?php

/**
 * Syte_StockSync
 */

declare(strict_types=1);

namespace Syte\StockSync\Plugins\Sales\Api;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\InventorySalesApi\Model\StockByWebsiteIdResolverInterface;
use Magento\InventorySalesApi\Model\GetStockItemDataInterface;
use Magento\InventoryConfigurationApi\Api\GetStockItemConfigurationInterface;
use Magento\InventoryReservationsApi\Model\GetReservationsQuantityInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Bundle\Model\Product\Type as Bundle;
use Syte\StockSync\Model\Helper;

class OrderManagementInterfacePlugin
{
    /**
     * @var StockByWebsiteIdResolverInterface
     */
    private $stockByWebsiteIdResolver;

    /**
     * @var GetStockItemDataInterface
     */
    private $getStockItemData;

    /**
     * @var GetStockItemConfigurationInterface
     */
    private $getStockItemConfiguration;

    /**
     * @var GetReservationsQuantityInterface
     */
    private $getReservationsQuantity;

    /**
     * @param StockByWebsiteIdResolverInterface $stockByWebsiteIdResolver
     * @param GetStockItemDataInterface $getStockItemData
     * @param GetStockItemConfigurationInterface $getStockItemConfiguration
     * @param GetReservationsQuantityInterface $getReservationsQuantity
     * @param Helper $helper
     */
    public function __construct(
        StockByWebsiteIdResolverInterface $stockByWebsiteIdResolver,
        GetStockItemDataInterface $getStockItemData,
        GetStockItemConfigurationInterface $getStockItemConfiguration,
        GetReservationsQuantityInterface $getReservationsQuantity,
        Helper $helper
    ) {
        $this->stockByWebsiteIdResolver = $stockByWebsiteIdResolver;
        $this->getStockItemData = $getStockItemData;
        $this->getStockItemConfiguration = $getStockItemConfiguration;
        $this->getReservationsQuantity = $getReservationsQuantity;
        $this->helper = $helper;
    }

    /**
     * @param OrderManagementInterface $subject
     * @param OrderInterface $order
     *
     * @return OrderInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\InventoryConfigurationApi\Exception\SkuIsNotAssignedToStockException
     */
    public function afterPlace(OrderManagementInterface $subject, OrderInterface $order): OrderInterface
    {
        if ($this->helper->isServiceAllowed()) {
            $websiteId = (int)$order->getStore()->getWebsiteId();
            $stock = $this->stockByWebsiteIdResolver->execute($websiteId);
            $stockId = (int)$stock->getStockId();
            $skuData = [];
            $deniedTypes = [Configurable::TYPE_CODE, Bundle::TYPE_CODE];
            foreach ($order->getAllItems() as $orderItem) {
                if (!in_array($orderItem->getProductType(), $deniedTypes)) {
                    if ($sku = $orderItem->getSku()) {
                        $stockItemData = $this->getStockItemData->execute($sku, $stockId);
                        $stockItemConfiguration = $this->getStockItemConfiguration->execute($sku, $stockId);
                        $qtyLeftInStock = $stockItemData[GetStockItemDataInterface::QUANTITY]
                           + $this->getReservationsQuantity->execute($sku, $stockId);
                        if (!$qtyLeftInStock || $this->helper->getConfigMinQty() > $qtyLeftInStock) {
                            $skuData[] = $sku;
                        }
                    }
                }
            }
            $this->helper->sendProductRequest($skuData);
        }

        return $order;
    }
}
