<?php

/**
 * Syte_StockSync
 */

declare(strict_types=1);

namespace Syte\StockSync\Model;

use Syte\Core\Model\Config as CoreConfig;
use Syte\Core\Model\Constants;

class Config extends CoreConfig
{
    /**
     * @const string
     */
    private const SYTE_CONFIG_AREA = Constants::SYTE_CONFIG_PATH_STOCK_SYNC;

    /**
     * Get config area value by short fieldname
     *
     * @param string $fieldShort
     * @param null|int $storeId
     *
     * @return null|int|string
     */
    private function getAreaConfig(string $fieldShort, $storeId = null)
    {
        return $this->getConfigValue(self::SYTE_CONFIG_AREA . $fieldShort, $storeId);
    }

    /**
     * Get active status
     *
     * @param int $storeId
     *
     * @return bool
     */
    public function isServiceActive(int $storeId): bool
    {
        $isActive = (int)$this->getAreaConfig('active', $storeId);

        return $isActive ? true : false;
    }

    /**
     * Get api endpoint
     *
     * @param int $storeId
     *
     * @return string
     */
    public function getApiEndpoint(int $storeId): string
    {
        $url = '';
        if ($this->isServiceActive($storeId) && $this->isAccountActive($storeId)) {
            $url = (string)$this->getAreaConfig('api_url', $storeId);
        }

        return $url;
    }

    /**
     * Get log status
     *
     * @param int $storeId
     *
     * @return bool
     */
    public function isLogActive(int $storeId): bool
    {
        $isActive = (int)$this->getAreaConfig('log', $storeId);

        return $isActive ? true : false;
    }
}
