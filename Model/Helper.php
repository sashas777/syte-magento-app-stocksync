<?php

/**
 * Syte_StockSync
 */

declare(strict_types=1);

namespace Syte\StockSync\Model;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\CatalogInventory\Api\StockStatusCriteriaInterfaceFactory;
use Magento\CatalogInventory\Api\StockStatusRepositoryInterface;
use Magento\CatalogInventory\Api\StockItemRepositoryInterface;
use Magento\CatalogInventory\Model\Stock\Item;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\HTTP\Client\Curl;
use Syte\Core\Model\Services\Scripts as ScriptHelper;
use Syte\StockSync\Model\Config as ConfigHelper;
use Psr\Log\LoggerInterface;
use Syte\Core\Model\Constants;

class Helper extends AbstractHelper
{
    /**
     * @var ConfigHelper
     */
    private $configHelper;

    /**
     * @var ScriptHelper
     */
    private $scriptHelper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var StockStatusCriteriaInterfaceFactory
     */
    private $stockStatusCriteriaFactory;

    /**
     * @var StockStatusRepositoryInterface
     */
    private $stockStatusRepository;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var Curl
     */
    private $curl;

    /**
     * @param Context $context
     * @param ConfigHelper $configHelper
     * @param ScriptHelper $scriptHelper
     * @param LoggerInterface $logHelper
     * @param StockStatusCriteriaInterfaceFactory $stockStatusCriteriaFactory
     * @param StockStatusRepositoryInterface $stockStatusRepository
     * @param ProductRepositoryInterface $productRepository
     * @param StoreManagerInterface $storeManager
     * @param SerializerInterface $serializer
     * @param Curl $curl
     */
    public function __construct(
        Context $context,
        ConfigHelper $configHelper,
        ScriptHelper $scriptHelper,
        LoggerInterface $logger,
        StockStatusCriteriaInterfaceFactory $stockStatusCriteriaFactory,
        StockStatusRepositoryInterface $stockStatusRepository,
        ProductRepositoryInterface $productRepository,
        StoreManagerInterface $storeManager,
        SerializerInterface $serializer,
        Curl $curl
    ) {
        $this->configHelper = $configHelper;
        $this->scriptHelper = $scriptHelper;
        $this->logger = $logger;
        $this->stockStatusCriteriaFactory = $stockStatusCriteriaFactory;
        $this->stockStatusRepository = $stockStatusRepository;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->serializer = $serializer;

        $this->curl = $curl;
        $this->curl->addHeader('Content-Type', 'application/json');

        parent::__construct($context);
    }

    /**
     * Get config notification min qty
     *
     * @return float
     */
    public function getConfigMinQty(): float
    {
        return (float)$this->configHelper->getConfigValue('cataloginventory/item_options/notify_stock_qty');
    }

    /**
     * Get allowed status
     *
     * @return bool
     */
    public function isServiceAllowed(): bool
    {
        $storeId = (int)$this->storeManager->getStore()->getId();

        return $this->configHelper->getAccountProductFeedName($storeId)
               && $this->configHelper->getApiEndpoint($storeId);
    }

    /**
     * Handle out-of-stock event
     *
     * @param Item $stockItem
     *
     * @return void
     */
    public function handleOutOfStock(Item $stockItem)
    {
        try {
            $product = $this->productRepository->getById($stockItem->getProductId());
        } catch (\Exception $e) {
            return;
        }
        if ($product) {
            $this->sendProductRequest([$product->getSku()]);
        }
    }

    /**
     * Get stock status item
     *
     * @param Item $stockItem
     *
     * @return null|\Magento\CatalogInventory\Model\Stock\Status
     */
    public function getStockStatusItem(Item $stockItem)
    {
        $websiteId = $stockItem->getWebsiteId();
        $productId = $stockItem->getProductId();
        $stockId = $stockItem->getStockId();
        $criteria = $this->stockStatusCriteriaFactory->create();
        $criteria->setProductsFilter($productId);
        $criteria->setScopeFilter($websiteId);
        $collection = $this->stockStatusRepository->getList($criteria);
        $stockStatuses = $collection->getItems();
        foreach ($stockStatuses as $stockStatus) {
            if ($stockStatus && $stockStatus->getProductId() && ($stockStatus->getStockId() == $stockId)) {
                return $stockStatus;
            }
        }

        return null;
    }

    /**
     * Send api request
     *
     * @param array $skuData
     *
     * @return mixed
     */
    public function sendProductRequest(array $skuData)
    {
        if (empty($skuData)) {
            return false;
        }
        $storeId = (int)$this->storeManager->getStore()->getId();
        $url = $this->configHelper->getApiEndpoint($storeId);
        $this->scriptHelper->replaceScriptContentAnchors($url, $storeId);
        $data = [];
        foreach ($skuData as $sku) {
            $data[] = ['sku' => $sku, 'syte_stock' => 'false'];
        }
        $logData = [__('Endpoint') . ': ' . $url];
        $logError = '';
        try {
            $options = [
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_CUSTOMREQUEST => 'PATCH',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_POSTFIELDS => $this->serializer->serialize($data),
            ];
            $this->curl->setOptions($options);

            $data['storeId'] = $storeId;
            $logData[] = $data;

            $this->curl->get($url);
            $result = $this->curl->getBody();

            $logData[] = __('Response') . ': ' . $this->serializer->serialize($result);
        } catch (\Exception $e) {
            $result = false;
            $logError = $e->getMessage();
        }
        if ($this->configHelper->isLogActive($storeId)) {
            $this->logger->debug('Syte_StockSync INFO', $logData);
            if ($logError) {
                $this->logger->debug('Syte_StockSync ERROR', is_array($logError) ? $logError : (array)$logError);
            }
        }

        return $result;
    }
}
