<?php

/**
 * Syte_StockSync
 */

declare(strict_types=1);

namespace Syte\StockSync\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Syte\StockSync\Model\Helper;

class StockItemSaveBefore implements ObserverInterface
{
    /**
     * @var Helper
     */
    private $helper;

    /**
     * @param Helper $helper
     */
    public function __construct(Helper $helper)
    {
        $this->helper = $helper;
    }

    /**
     *  Observer's executable
     *
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(Observer $observer)
    {
        if ($this->helper->isServiceAllowed()) {
            if ($stockItem = $observer->getEvent()->getItem()) {
                if ($stockStatus = $this->helper->getStockStatusItem($stockItem)) {
                    $qty = $stockItem->getQty();
                    if ((!$qty || ($qty < $this->helper->getConfigMinQty()))
                        && ($stockStatus->getStockStatus() || $stockItem->getIsInStock())
                        && ($qty != $stockStatus->getQty())
                    ) {
                        $this->helper->handleOutOfStock($stockItem);
                    }
                }
            }
        }
    }
}
